﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class Boton_Inicio : MonoBehaviour
{
    public Text scores;
    // Use this for initialization
    void Start()
    {
        if (scores != null)
        {
            scores.text = "Reto actual : " + PlayerPrefs.GetInt("Player Score").ToString();

        }
    }
    public void Jugar()
    {
        SceneManager.LoadScene("Intro");
    }

    public void Inicio()
    {
        SceneManager.LoadScene("Title");
    }


    public void Creditos()
    {
        SceneManager.LoadScene("Credits");
    }
}
